# upskilling-concept

## What?

A draft upskilling plan to help colleagues develop themselves in preparation for the release of the new tech stack. This will undergo an iterative approach and will eventually be communicated to colleagues at censhare for further feedback and adaption.

## Why? 

Colleagues often reach out and ask "How do you recommend we upskill ourselves in the new tech stack?". Quite truthfully, I don't believe there is a quick or easy answer to this question. Transitioning from Function Object Oriented Programming to Scripting languages is a process which involves studying and learning by doing. Therefore, to help 'ease our SolDevs into this change' we’ll use this repository to suggest steps on prerequisite technologies, best practice, and industry standards. 

This is also a learning process for me as I aim to use and build familiarity with CLI in combination with Git workflows. 

## How? 

Ideally using a blended approach, where our Solution Developers work in tandem with our SCRUM teams. However, given the change to remote-working and pending project deadlines, such a _synchronous_ concept isn't ideal. Documenting how best to upskill in this fashion, where it is open to MRs and external influence, enables colleagues and SMEs alike to propose changes, share experiences and feel invested from the outset. 
